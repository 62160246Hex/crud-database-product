/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
/**
 *
 * @author Code.Addict
 */
public class Bridge {

	static void print(String x){
		System.out.println(x);
	}

	private static Connection bridge = null;

	public static Connection createBridge() {
		String dbPath = "./db/store.db";

		try {
			Class.forName("org.sqlite.JDBC");
			bridge = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
			print("[ Info ] Bridge connected.");
		} catch (ClassNotFoundException ex) {
			print("[ Error ] JDBC is not exist.");
		} catch (SQLException ex) {
			print("[ Error ] Bridge can't connect.");
		}

		return bridge;
	}

	public static void closeBridge() {
		try {
			if (bridge != null) {
				bridge.close();
				print("[ Info ] Close Bridge successfully.");
			}
		} catch (SQLException ex) {
			print("[ Error ] Can't close Bridge.");
		}
	}
}
