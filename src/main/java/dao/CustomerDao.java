/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.hex.store.poc.Utils;
import database.Bridge;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;

/**
 *
 * @author Code.Addict
 */
public class CustomerDao implements DaoInterface<Customer> {

	public int add(Customer object) {
		Connection bridge = Bridge.createBridge();

		try {
			String queryStr = "INSERT INTO customer(name, tel) VALUES(?, ?)";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setString(2, object.getTel());
			int row = stmt.executeUpdate();
			ResultSet data = stmt.getGeneratedKeys();
			if (data.next()) {
				System.out.printf("[ Updated Customer ] RowChanged(%d)\nId(%d)\n", row, data.getInt(1));
			}
			return data.getInt(1);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		Bridge.closeBridge();
		return 0;
	}

	@Override
	public ArrayList<Customer> getAll() {

		ArrayList list = new ArrayList();
		Connection bridge = Bridge.createBridge();
		try {

			String queryStr = "SELECT * FROM customer";
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			while (data.next()) {
				int id = data.getInt("id");
				String name = data.getString("name");
				String tel = data.getString("tel");
				list.add(new Customer(id, name, tel));
			}
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return list;
	}

	@Override
	public Customer get(int id) {
		Connection bridge = Bridge.createBridge();
		Customer result = null;
		try {

			String queryStr = "SELECT * FROM customer WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			result = new Customer(data.getInt("id"), data.getString("name"), data.getString("tel"));
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return result;
	}

	@Override
	public void delete(int id) {
		Connection bridge = Bridge.createBridge();
		try {
			String queryStr = "DELETE FROM customer WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			int rowChanged = stmt.executeUpdate(queryStr);
			Utils.print("[ Deleted Customer ] " + rowChanged);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();
	}

	@Override
	public int update(Customer object) {
		Connection bridge = Bridge.createBridge();

		/* Process */
		try {
			String queryStr = "UPDATE customer SET name = ?, tel = ? WHERE id = ?";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setString(2, object.getTel());
			stmt.setInt(3, object.getId());
			int row = stmt.executeUpdate();
			return row;
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		/* Process */
		Bridge.closeBridge();
		return 0;
	}

	public static void main(String[] args) {
		CustomerDao dao = new CustomerDao();
		System.out.println(dao.getAll());
		System.out.println(dao.get(6));
		dao.delete(7);
		int id = dao.add(new Customer(-1, "roblox", "+66958289478"));
		System.out.println("id: " + id);
		Customer lastCustomer = dao.get(id);
		System.out.println("Last Customer: " + lastCustomer);
		lastCustomer.setName("Oh this not hard");
		dao.update(lastCustomer);
		Customer updateCustomer = dao.get(id);
		System.out.println("Update Customer: " + updateCustomer);
		dao.delete(id);
		Customer deleteCustomer = dao.get(id);
		System.out.println("delete Customer: " + deleteCustomer);
	}

}
