/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.hex.store.poc.Utils;
import database.Bridge;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;
import model.User;

/**
 *
 * @author Code.Addict
 */
public class UserDao implements DaoInterface<User> {

	public int add(User object) {
		Connection bridge = Bridge.createBridge();

		try {
			String queryStr = "INSERT INTO user(name, password, tel) VALUES(?, ?, ?)";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setString(2, object.getPassword());
			stmt.setString(2, object.getTel());
			int row = stmt.executeUpdate();
			ResultSet data = stmt.getGeneratedKeys();
			if (data.next()) {
				System.out.printf("[ Updated User ] RowChanged(%d)\nId(%d)\n", row, data.getInt(1));
			}
			return data.getInt(1);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		Bridge.closeBridge();
		return 0;
	}

	@Override
	public ArrayList<User> getAll() {

		ArrayList list = new ArrayList();
		Connection bridge = Bridge.createBridge();
		try {

			String queryStr = "SELECT * FROM user";
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			while (data.next()) {
				int id = data.getInt("id");
				String name = data.getString("name");
				String password = data.getString("password");
				String tel = data.getString("tel");
				list.add(new User(id, name, password, tel));
			}
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return list;
	}

	@Override
	public User get(int id) {
		Connection bridge = Bridge.createBridge();
		User result = null;
		try {

			String queryStr = "SELECT * FROM user WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			result = new User(data.getInt("id"), data.getString("name"), data.getString("password"), data.getString("tel"));
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();

		return result;
	}

	@Override
	public void delete(int id) {
		Connection bridge = Bridge.createBridge();
		try {
			String queryStr = "DELETE FROM user WHERE id = " + id;
			Statement stmt = bridge.createStatement();
			int rowChanged = stmt.executeUpdate(queryStr);
			Utils.print("[ Deleted User ] " + rowChanged);
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}
		Bridge.closeBridge();
	}

	@Override
	public int update(User object) {
		Connection bridge = Bridge.createBridge();

		/* Process */
		try {
			String queryStr = "UPDATE user SET name = ?, password = ?, tel = ? WHERE id = ?";
			PreparedStatement stmt = bridge.prepareStatement(queryStr);
			stmt.setString(1, object.getName());
			stmt.setString(2, object.getPassword());
			stmt.setString(3, object.getTel());
			stmt.setInt(4, object.getId());
			int row = stmt.executeUpdate();
			return row;
		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		/* Process */
		Bridge.closeBridge();
		return 0;
	}

	public static void main(String[] args) {
		UserDao dao = new UserDao();
		System.out.println(dao.getAll());
		System.out.println(dao.get(6));
		dao.delete(7);
		int id = dao.add(new User(-1, "local", "password", "+66958289478"));
		System.out.println("id: " + id);
		User lastUser = dao.get(id);
		System.out.println("Last User: " + lastUser);
		lastUser.setPassword("Oh this not hard");
		dao.update(lastUser);
		User updateUser = dao.get(id);
		System.out.println("Update User: " + updateUser);
		dao.delete(id);
		User deleteUser = dao.get(id);
		System.out.println("delete User : " + deleteUser);
	}

}
