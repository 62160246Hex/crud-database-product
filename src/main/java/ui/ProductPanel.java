/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import dao.ProductDao;
import java.util.ArrayList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import model.Product;

/**
 *
 * @author Code.Addict
 */
public class ProductPanel extends javax.swing.JPanel {
	
	private ArrayList<Product> productList;
	private ProductTableModel model;
	private Product editingProduct;
	private boolean isEnabledDebug = true;

	void debug(String values){	
		if(!isEnabledDebug)return;
		System.out.println("[ debug ] "+ values);
	}

	public ProductPanel() {
		initComponents();
		ProductDao dao = new ProductDao();
		loadTable(dao);
	}
	
	
	private void loadTable(ProductDao dao){
		productList = dao.getAll();
		model = new ProductTableModel(productList);
		tableProduct.setModel(model);

		tableProduct.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e) {
				editingProduct = productList.get(tableProduct.getSelectedRow());
				loadProductToForm();
				toggleButtonDelete(true);
				toggleButtonAdd(false);
				toggleButtonSaveAndCancel(true);
			}
		});
	}
	
	private void refreshTable(){
		ProductDao dao = new ProductDao();
		ArrayList<Product> newList = dao.getAll();
		productList.clear();
		productList.addAll(newList);
		tableProduct.revalidate();
		tableProduct.repaint();
	}
	
	private void clearEditFrom(){
		editingProduct = null;
		labelId.setText("");
		fieldName.setText("");
		fieldPrice.setText("");
	}

	private void loadProductToForm(){
		labelId.setText(""+editingProduct.getId());
		fieldName.setText(editingProduct.getName());
		fieldPrice.setText(""+editingProduct.getPrice());
	}
	
	private void loadFromToProduct(){
		editingProduct.setName(fieldName.getText());
		editingProduct.setPrice(Double.parseDouble(fieldPrice.getText()));

		ProductDao dao = new ProductDao();
		if(editingProduct.getId() != -1){
			dao.update(editingProduct);
		}else{
			dao.add(editingProduct);
		}
	
		refreshTable();
		debug("Saved Product -> "+ editingProduct.toString());
		toggleButtonSaveAndCancel(false);
		clearEditFrom();
	}

	private void toggleButtonAdd(boolean isEnable){
		btnAdd.setEnabled(isEnable);
	}
	
	private void toggleButtonDelete(boolean isEnable){
		btnDelete.setEnabled(isEnable);
	}

	private void toggleButtonSaveAndCancel(boolean isEnable){
		btnSave.setEnabled(isEnable);
		btnCancel.setEnabled(isEnable);
	}
	
	private void deleteProduct(){
		ProductDao dao = new ProductDao();
		dao.delete(editingProduct.getId());
		clearEditFrom();
		toggleButtonSaveAndCancel(false);
		toggleButtonAdd(true);
		toggleButtonDelete(false);
		refreshTable();
		editingProduct = null;

	}

	@SuppressWarnings("unchecked")
     // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
     private void initComponents() {

          jPanel1 = new javax.swing.JPanel();
          jLabel1 = new javax.swing.JLabel();
          labelId = new javax.swing.JLabel();
          jLabel2 = new javax.swing.JLabel();
          jLabel3 = new javax.swing.JLabel();
          fieldName = new javax.swing.JTextField();
          fieldPrice = new javax.swing.JTextField();
          btnCancel = new javax.swing.JButton();
          btnSave = new javax.swing.JButton();
          jPanel2 = new javax.swing.JPanel();
          btnDelete = new javax.swing.JButton();
          btnAdd = new javax.swing.JButton();
          jScrollPane1 = new javax.swing.JScrollPane();
          tableProduct = new javax.swing.JTable();

          jLabel1.setText("ID :");

          jLabel2.setText("Name :");

          jLabel3.setText("Price :");

          fieldName.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    fieldNameActionPerformed(evt);
               }
          });

          fieldPrice.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    fieldPriceActionPerformed(evt);
               }
          });

          btnCancel.setText("Cancel");
          btnCancel.setEnabled(false);
          btnCancel.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnCancelActionPerformed(evt);
               }
          });

          btnSave.setText("Save");
          btnSave.setEnabled(false);
          btnSave.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnSaveActionPerformed(evt);
               }
          });

          javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
          jPanel1.setLayout(jPanel1Layout);
          jPanel1Layout.setHorizontalGroup(
               jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                         .addGroup(jPanel1Layout.createSequentialGroup()
                              .addComponent(jLabel1)
                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(labelId, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                         .addGroup(jPanel1Layout.createSequentialGroup()
                              .addComponent(jLabel2)
                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(fieldName))
                         .addGroup(jPanel1Layout.createSequentialGroup()
                              .addComponent(jLabel3)
                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                   .addComponent(fieldPrice)
                                   .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(btnSave)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnCancel)))))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          jPanel1Layout.setVerticalGroup(
               jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                         .addComponent(jLabel1)
                         .addComponent(labelId, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                         .addComponent(jLabel2)
                         .addComponent(fieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                         .addComponent(jLabel3)
                         .addComponent(fieldPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                         .addComponent(btnCancel)
                         .addComponent(btnSave))
                    .addGap(0, 10, Short.MAX_VALUE))
          );

          btnDelete.setText("Delete");
          btnDelete.setEnabled(false);
          btnDelete.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnDeleteActionPerformed(evt);
               }
          });

          btnAdd.setText("Add");
          btnAdd.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    btnAddActionPerformed(evt);
               }
          });

          javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
          jPanel2.setLayout(jPanel2Layout);
          jPanel2Layout.setHorizontalGroup(
               jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(btnAdd)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnDelete)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          jPanel2Layout.setVerticalGroup(
               jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                         .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                         .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
          );

          tableProduct.setModel(new javax.swing.table.DefaultTableModel(
               new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
               },
               new String [] {
                    "Title 1", "Title 2", "Title 3", "Title 4"
               }
          ));
          jScrollPane1.setViewportView(tableProduct);

          javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
          this.setLayout(layout);
          layout.setHorizontalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                         .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                         .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1072, Short.MAX_VALUE))
                    .addContainerGap())
          );
          layout.setVerticalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 318, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
     }// </editor-fold>//GEN-END:initComponents

     private void fieldNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldNameActionPerformed
          // TODO add your handling code here:
     }//GEN-LAST:event_fieldNameActionPerformed

     private void fieldPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fieldPriceActionPerformed
          // TODO add your handling code here:
     }//GEN-LAST:event_fieldPriceActionPerformed

     private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
		clearEditFrom();
		toggleButtonSaveAndCancel(false);
		toggleButtonAdd(true);
		toggleButtonDelete(false);
		tableProduct.setEnabled(true);
     }//GEN-LAST:event_btnCancelActionPerformed

     private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
		deleteProduct();
     }//GEN-LAST:event_btnDeleteActionPerformed

     private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
		loadFromToProduct();
		toggleButtonAdd(true);
		toggleButtonDelete(false);
		tableProduct.setEnabled(true);
     }//GEN-LAST:event_btnSaveActionPerformed

     private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
		clearEditFrom();
		editingProduct = new Product(-1, "", 0.00);
		toggleButtonAdd(false);
		tableProduct.setEnabled(false);
		toggleButtonSaveAndCancel(true);
     }//GEN-LAST:event_btnAddActionPerformed


     // Variables declaration - do not modify//GEN-BEGIN:variables
     private javax.swing.JButton btnAdd;
     private javax.swing.JButton btnCancel;
     private javax.swing.JButton btnDelete;
     private javax.swing.JButton btnSave;
     private javax.swing.JTextField fieldName;
     private javax.swing.JTextField fieldPrice;
     private javax.swing.JLabel jLabel1;
     private javax.swing.JLabel jLabel2;
     private javax.swing.JLabel jLabel3;
     private javax.swing.JPanel jPanel1;
     private javax.swing.JPanel jPanel2;
     private javax.swing.JScrollPane jScrollPane1;
     private javax.swing.JLabel labelId;
     private javax.swing.JTable tableProduct;
     // End of variables declaration//GEN-END:variables

	private class ProductTableModel extends AbstractTableModel {

		private final ArrayList<Product> data;
		String[] columnName = {"ID" , "Name", "Price"};

		public ProductTableModel(ArrayList<Product> data){
			
			this.data = data;

		}

		public int getRowCount() {
			return this.data.size();
		}

		public int getColumnCount() {
			return 3;
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			Product product = this.data.get(rowIndex);

			if(columnIndex == 0){
				return product.getId();
			}

			if(columnIndex == 1){
				return product.getName();
			}

			if(columnIndex == 2){
				return product.getPrice();
			}
			
			return "";
		}

		@Override
		public String getColumnName(int column) {
			return columnName[column];
		}
		

	}

}
