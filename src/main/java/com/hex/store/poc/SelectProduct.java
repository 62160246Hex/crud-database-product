/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hex.store.poc;

import database.Bridge;
import java.sql.*;

/**
 *
 * @author Code.Addict
 */
public class SelectProduct {

	public static void main(String[] args) {
		Connection bridge = Bridge.createBridge();
		/* Process */

		try {

			String queryStr = "SELECT * FROM product";
			Statement stmt = bridge.createStatement();
			ResultSet data = stmt.executeQuery(queryStr);
			while (data.next()) {
				int id = data.getInt("id");
				String name = data.getString("name");
				double price = data.getDouble("price");
				System.out.printf("\nId(%d) Name(%s) Price(%.2f)",
					id, name, price);
			}

		} catch (SQLException ex) {
			Utils.print("[ Error ] " + ex);
		}

		/* Process */
		Bridge.closeBridge();
	}
}
